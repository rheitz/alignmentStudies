#from ROOT import gROOT, TCanvas, TH1F
import ROOT, math, re
from array import array
from detectorHelpers import read_file


def fill_hist (x, bins, h_min, h_max, multiplier):
    '''
    x: list, of values to fill histogram with
    bins: int, number of bins
    h_min: int, minimum x value
    h_max: int, maximum x value
    multiplier: int, multiple filled valued by this number
    returns: TH1F,
    '''
    h=ROOT.TH1F("name", "title", bins, h_min, h_max)
    for i in range(len(x)):
        h.Fill(multiplier * x[i])
    return h

def tgraphE_setup (x, y, ex, ey, titles):
    '''
    x: list [int or float], of x values
    y: list [int or float], of y values
    ex: list [int or float], of errors for x
    ey: list [int or float], of errors for y
    titles: list [str], [0] title, [1] x-axis, [2] y-axis
    returns: TGraphErrors, set up nicely
    '''
    assert len(x) == len(y) == len(ex) == len (ey)
    xArray = array('f', x)
    yArray = array('f', y)
    exArray = array('f', ex)
    eyArray = array('f', ey)
    gr = ROOT.TGraphErrors(len(x), xArray, yArray, exArray, eyArray)
    gr.SetTitle(titles[0])
    gr.GetXaxis().SetTitle(titles[1])
    gr.GetYaxis().SetTitle(titles[2])
    gr.SetMarkerColor(4)
    gr.SetMarkerStyle(21)
    return gr

def diff (orig, new):
    '''
    orig: list, originial values
    new: list, new values
    returns: list of orippg
    '''
    return_list = []
    for i in range(len(orig)):
        return_list.append(float(orig[i]) - float(new[i]))

    return return_list

def hist_props (hist, title, xAxis = "microns"):
    '''
    Sets up generic properties of a histgram to be drawn
    hist: TH#, histogram to define properties for
    title: str
    xAxis: str, title of x-axis
    '''
    hist.SetTitle(title)
    hist.GetXaxis().SetTitle(xAxis)

def hist_stats (hist):
    '''
    hist: class similar_histograms
    returns: list of list of int. First list is mean, 2nd rms
    '''
    hist_stat = []
    hist_mean = []
    hist_rms = []
    for i in range(len(hist)):
        hist_mean.append(hist.hists[i].GetMean())
        hist_rms.append(hist.hists[i].GetRMS())
    hist_stat.append(hist_mean)
    hist_stat.append(hist_rms)

    return hist_stat

########## NOT USED ##############
#def canvas_pads (numPads, padOption = 0):
#    '''
#    Sets up canvas with numPads of pads.  
#    Canvas is named c1, pads are names pad1, pad2 ...
#    numPads: int
#    padOption: int, 0 = pads horizontal side by side, 1 pads on top of each other
#    '''
#    c1 = ROOT.TCanvas('c1', 'c1', 1600, 800)
#    ROOT.gStyle.SetOptStat("rme")
#    offSet = 0.05 #border offset for pads
#    padVariables = {}
#    for i in range(numPads):
#        if padOption == 0:        
#            padVariables["pad{0}".format(i)] = ROOT.TPad("pad{0}".format(i), "pad{0}".format(i), offSet + float(i)/numPads, offSet,
#                                                         float((i+1))/numPads - float(offSet)/2, 1 - offSet, 21)
#            padVariables["pad{0}".format(i)].Draw()
#            print str(offSet + float(i)/numPads) + ' ' +  str(offSet) + ' ' + str(float((i+1))/numPads - float(offSet)/2) + ' ' + str(1 - offSet)
#        elif padOption == 1:
#            print "don't do anything"
#            #name = ROOT.TPad(name, name,
#        else:
#            print "Invalid padOption"

def large_change (rDiffs, cutOff):
    '''
    Finds detectors which change larger than cutOff 
    rDiffs: list of float or int, variables to look for large changes
    cutOff: float or int, stores detector names with changes larger than cutOff
    returns: list, of detectors with large movements and the amount they moved
    Warning if there are many values with the same change
         only the first value will be saved
    '''
    #Can be any detectors.dat
    tbNames = read_file('detectors.orig.dat', 2)
    bigMoves = []
    for val in rDiffs:
        if val > cutOff:
            try:
                ind = rDiffs.index(val)
            except ValueError:
                print "Couldn't find " + str(val) + " something went wrong!"
            bigMoves.append(tbNames[ind] + '      ' + str(val))
    return bigMoves

def large_change (valOrig, valNew, cutOff):
    '''
    Finds detectors which change larger than cutOff 
    valOrig: list of float or int, variables to compare difference of
    valNew: list or float or int,
    cutOff: float or int, stores detector names with changes larger than cutOff
    returns: list, of detectors with large movements
    '''
    change = diff(valOrig, valNew)
    return large_change(change, cutOff)

def small_change (rDiffs, cutOff):
    '''
    Finds detectors which change less than equal to cutOff 
    rDiffs: list of float or int, variables to look for small changes
    cutOff: float or int, stores detector names with changes less than equal to cutOff
    returns: list, of detectors with small movements and the amount they moved
    '''
    #Can be any detectors.dat
    tbNames = read_file('detectors.orig.dat', 2)
    smallMoves = []
    index = 0
    for val in rDiffs:
        if val <= cutOff:
            smallMoves.append(tbNames[index] + '      ' + str(val))
        index += 1
    return smallMoves

def radial_diff (xValueDiff, yValueDiff):
    '''
    Finds the radial changes for each detector
    xValueDiff: list of floats
    yValueDiff: list of floats
    returns: list, of radial differences
    '''
    assert len(xValueDiff) == len(yValueDiff)
    radial_diff = []
    for i in range(len(xValueDiff)):
        radial_diff.append(math.sqrt(xValueDiff[i]**2 + yValueDiff[i]**2))
    return radial_diff

class Variables(object):

    def __init__(self, detFilesOrig, detFilesNew):
        '''
        detFilesOrig: list, original detectors.dats
        detFilesNew: list, new detectors.dats
        The itea is to compare "orig" to "new"
        '''
        ### Orig variables
        self.lenFilesOrig = len(detFilesOrig)
        self.xPositionsOrig = {}
        self.yPositionsOrig = {}
        self.angleOrig = {}
        self.pitchOrig = {}
        self.wireDistOrig = {}
        self.zPositionsOrig = {}
        self.rDiffOrig = {}

        ### New variables
        self.lenFilesNew = len(detFilesNew)
        self.xPositionsNew = {}
        self.yPositionsNew = {}
        self.angleNew = {}
        self.pitchNew = {}
        self.wireDistNew = {}
        self.zPositionsNew = {}
        self.rDiffNew = {}

        ### make list of variables
        for i in range(len(detFilesOrig)):
            self.xPositionsOrig[i] = read_file(detFilesOrig[i], 11, 'det')
            self.yPositionsOrig[i] = read_file(detFilesOrig[i], 12, 'det')
            self.angleOrig[i] = read_file(detFilesOrig[i], 15, 'det')
            self.pitchOrig[i] = read_file(detFilesOrig[i], 17, 'det')
            self.wireDistOrig[i] = read_file(detFilesOrig[i], 14, 'det')
            self.zPositionsOrig[i] = read_file(detFilesOrig[i], 10, 'det')

        for i in range(len(detFilesNew)):
            self.xPositionsNew[i] = read_file(detFilesNew[i], 10, 'dead')
            self.yPositionsNew[i] = read_file(detFilesNew[i], 11, 'dead')
            self.angleNew[i] = read_file(detFilesNew[i], 15, 'det')
            self.pitchNew[i] = read_file(detFilesNew[i], 17, 'det')
            self.wireDistNew[i] = read_file(detFilesNew[i], 14, 'det')
            self.zPositionsNew[i] = read_file(detFilesNew[i], 9, 'dead')

    
    def getX(self, n, option = 'Orig'):
        '''
        n: int, x position variables
        option: str, Orig or New
        returns a copy of list of x positions
        '''
        if option == 'Orig':
            return self.xPositionsOrig[n][:]
        elif option == 'New':
            return self.xPositionsNew[n][:]
        else:
            print "wrong option"

    def getY(self, n, option = 'Orig'):
        '''
        n: int, y position variables
        option: str, Orig or New
        returns a copy of list of y positions
        '''
        if option == 'Orig':
            return self.yPositionsOrig[n][:]
        elif option == 'New':
            return self.yPositionsNew[n][:]
        else:
            print "wrong option"

    def rDiff (self):
        '''
        Fills rDiffOrig and rDiffNew dictionaries
        '''
        for i in range(self.lenFilesOrig - 1):
            self.rDiffOrig[i] = radial_diff(diff(self.getX(i), self.getX(i+1)), diff(self.getY(i), self.getY(i+1)))

        for i in range(self.lenFilesNew - 1):
            self.rDiffNew[i] = radial_diff(diff(self.getX(i, 'New'), self.getX(i+1, 'New')), diff(self.getY(i, 'New'), self.getY(i+1, 'New')))

    def getR(self, n, option = 'Orig'):
        '''
        n: int, radial change variables
        option: str, Orig or New
        returns a copy of list of y positions
        '''
        self.rDiff()
        if option == 'Orig':
            return self.rDiffOrig[n][:]
        elif option == 'New':
            return self.rDiffNew[n][:]
        else:
            print "wrong option"
    
    def getAngle(self, n, option = 'Orig'):
        '''
        n: int, angle variables position
        option: str, Orig or New
        returns a copy of list of angle variables
        '''
        if option == 'Orig':
            return self.angleOrig[n][:]
        elif option == 'New':
            return self.angleNew[n][:]
        else:
            print "wrong option"

    def getPitch(self, n, option = 'Orig'):
        '''
        n: int, pitch variables position
        option: str, Orig or New
        returns a copy of list of pitch variables
        '''
        if option == 'Orig':
            return self.pitchOrig[n][:]
        elif option == 'New':
            return self.pitchNew[n][:]
        else:
            print "wrong option"

    def getWireDist(self, n, option = 'Orig'):
        '''
        n: int, wire distance variables position
        option: str, Orig or New
        returns a copy of list of pitch variables
        '''
        if option == 'Orig':
            return self.wireDistOrig[n][:]
        elif option == 'New':
            return self.wireDistNew[n][:]
        else:
            print "wrong option"

    def getZ(self, n, option = 'Orig'):
        '''
        n: int, z position variables
        option: str, Orig or New
        returns a copy of list of z positions
        '''
        if option == 'Orig':
            return self.zPositionsOrig[n][:]
        elif option == 'New':
            return self.zPositionsNew[n][:]
        else:
            print "wrong option"


class canvas_pads(object):
    
    def __init__(self, nCanvas, mDivide, canvas0 = 0, canOption = 'rmeuo'):
        '''
        Sets up n canvases
        nCanvas: int, number of canvases
        mDivide: int, divide canvas m times
        canOption: str, options for global stat box
        '''
        self.canvas = {}
        for i in range(canvas0, nCanvas + canvas0):
            self.canvas[i - canvas0] = ROOT.TCanvas("c{0}".format(i), "c{0}".format(i), 1600, 800)
            self.canvas[i - canvas0].Divide(mDivide, 1, 0.01, 0.01, 21)
        ROOT.gStyle.SetOptStat(canOption)
        ROOT.gStyle.SetStatW(0.4);
        ROOT.gStyle.SetStatH(0.4);
     

class similar_histograms(object):
    '''
    For making histograms of the same lay out
    '''
    def __init__(self, nHist, props, option = 'Diff'):
        '''
        nHist: int, number of histograms
        props: list; variable (dictionary), bin size (int), hist min (int), hist max (int), hist multiplier (int), title (str), axis labels (str)
        option: str, Diff for difference histograms, Norm for using data straight from props
        '''
        self.nHist = nHist
        self.hists = {}
        if option == 'Diff':
            for i in range(nHist):
                self.hists[i] = fill_hist(diff(props[0][i], props[0][i+1]), props[1], props[2], props[3], props[4])
                hist_props (self.hists[i], props[5], props[6])
        elif option == 'Norm':
            for i in range(nHist):
                self.hists[i] = fill_hist(props[0][i], props[1], props[2], props[3], props[4])
                hist_props (self.hists[i], props[5])
        else:
            print "Wrong option for similar_histograms"

    def __len__(self):
        '''
        Gives length = number of histograms in class
        '''
        return self.nHist

#########################################
#########User Interface Below############
#########################################


#### detector files to analyze
detectorsOrig = ['detectors.ai0.dat', 'detectors.ai1.dat', 
                 'detectors.ai2.dat', 'detectors.ai3.dat', 'detectors.ai4.dat'] #variables from 'det' lines
detectorsNew =  ['detectors.orig.dat', 'detectors.new.dat'] #variables from 'dead' lines

#### Variables
var = Variables(detectorsOrig, detectorsNew)

#### Hist Canvases
nCanvas = 2
mDivide = 4
canvases = canvas_pads(nCanvas, mDivide)

#### Set up and draw histograms
i_canvasHist = 0

## X position
getX = {0:var.getX(0), 1:var.getX(1), 2:var.getX(2), 3:var.getX(3), 4:var.getX(4)}
xProps = [getX, 100, -500, 500, 10000, "X position", 'microns']

histX = similar_histograms(4, xProps)
for i in range(mDivide):
    canvases.canvas[i_canvasHist].cd(i+1)
    histX.hists[i].Draw()
i_canvasHist += 1

## Y position
getY = {0:var.getY(0), 1:var.getY(1), 2:var.getY(2), 3:var.getY(3), 4:var.getY(4)}
yProps = [getY, 100, -500, 500, 10000, "Y position", 'microns']

histY = similar_histograms(4, yProps)
for i in range(mDivide):
    canvases.canvas[i_canvasHist].cd(i+1)
    histY.hists[i].Draw()
i_canvasHist += 1

### Angle
#getAngle = {0:var.getAngle(0), 1:var.getAngle(1), 2:var.getAngle(2), 3:var.getAngle(3), 4:var.getAngle(4)}
#angleProps = [getAngle, 100, -50, 50, 1000, "Angle", "millidegrees"]
#
#histAngle = similar_histograms(4, angleProps)
#for i in range(mDivide):
#    canvases.canvas[i_canvasHist].cd(i+1)
#    histAngle.hists[i].Draw()
#i_canvasHist += 1
#
### Pitch
#getPitch = {0:var.getPitch(0), 1:var.getPitch(1), 2:var.getPitch(2), 3:var.getPitch(3), 4:var.getPitch(4)}
#pitchProps = [getPitch, 100, -5, 5, 10000, "Pitch", "microns"]
#
#histPitch = similar_histograms(4, pitchProps)
#for i in range(mDivide):
#    canvases.canvas[i_canvasHist].cd(i+1)
#    histPitch.hists[i].Draw()
#i_canvasHist += 1

### Wire Distance
#getWireDist = {0:var.getWireDist(0), 1:var.getWireDist(1), 2:var.getWireDist(2), 3:var.getWireDist(3), 4:var.getWireDist(4)}
#wireDistProps = [getWireDist, 100, -100, 100, 10000, "Wire Distance", "microns"]
#
#histWireDist = similar_histograms(4, wireDistProps)
#for i in range(mDivide):
#    canvases.canvas[i_canvasHist].cd(i+1)
#    histWireDist.hists[i].Draw()
#i_canvasHist += 1

## Radial change
#getR = {0:var.getR(0), 1:var.getR(1), 2:var.getR(2), 3:var.getR(3)}
#rProps = [getR, 100, 0, 1000, 10000, "R position", 'microns']
#
#histR = similar_histograms(4, rProps, 'Norm')
#for i in range(mDivide):
#    canvases.canvas[i_canvasHist].cd(i+1)
#    histR.hists[i].Draw()
#i_canvasHist += 1

### TGraphError Canvases
pnCanvas = 2
mDivide = 1
cTGraphE = canvas_pads(nCanvas, mDivide, i_canvasHist)

#### Set up and draw TGraphErrors
i_canvasGraphE = 0

###TGraph Errors
ai = [1, 2, 3, 4]
ex = [0, 0, 0, 0]

## X position changes
cTGraphE.canvas[i_canvasGraphE].cd()
statX = hist_stats(histX)
titlesX = ['X changes, Error bars = rms', 'iterations', 'microns']
xChanges = tgraphE_setup(ai, statX[0], ex, statX[1], titlesX)
xChanges.Draw("APL")
i_canvasGraphE += 1

## Y position changes
cTGraphE.canvas[i_canvasGraphE].cd()
statY = hist_stats(histY)
titlesY = ['Y changes, Error bars = rms', 'iterations', 'microns']
yChanges = tgraphE_setup(ai, statY[0], ex, statY[1], titlesY)
yChanges.Draw("APL")
i_canvasGraphE += 1

#### Other interesting variables
#bigMoves = large_change(r_diff, 0.1)
bigMoves = large_change(var.getR(0), 0.1)
noMoves = small_change(var.getR(0), 0.000)
smallMoves = small_change(var.getR(0), 0.01)

