import re

#### Not read detectors i.e. dets that don't move in alignment
offDets = ['H', 'V', 'FI', 'ST0....c', 'ST0....a', 
           'GM06X', 'GM06Y', 'GP..P.'] #spectrometer current setting
#offDets = ['H', 'V', 'FI'] #spectrometer
#offDets = ['H', 'V', 'FI35', 'FI04', 'ST', 'G', 'D', 'M', 'P'] #BT
#offDets = ['H', 'V', 'FI0', 'FI1', 'ST', 'G', 'D', 'M', 'P'] #vtx
#offDets = 0 #Everything
#offDets =  ['H', 'V'] #Everything that can move
def read_file (detectors, fieldnum, lineType = 'det', notRead = offDets):
    '''
    detetectors: file, file to be read (assumed to be in geometry folder)
           looking only at detectors.  Lines that start with det.
    fieldnum: int, field number to store
    det: str, det or dead
    notRead: list or int, list of detectors not to read or 0 to read all
    returns: list, all entries from number field fieldnum
    '''
    return_field = []
    f = open ('geometry/'+detectors, 'r')
    for line in f:
        if len(line) > 10 and line[1:4] == lineType:
            detFields = line.split()
            if notRead == 0:
                return_field.append(detFields[fieldnum])
            elif len(notRead) > 0:
                for i in range(len(notRead)):
                    if re.match(notRead[i], detFields[2]) != None:
                        break
                else:
                    return_field.append(detFields[fieldnum])
            else:
                print 'Wrong notRead in read_file'

    return return_field
